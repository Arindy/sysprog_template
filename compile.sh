#!/bin/bash

rm -rf out
mkdir out
cd out || exit 1
echo "=== Starting first run of pdflatex with $1.tex ..."
pdflatex -file-line-error -interaction=nonstopmode -synctex=1 -output-format=pdf -include-directory=../src -shell-escape "$1".tex
echo "=== Starting run of bibtex for $1 ..."
bibtex -include-directory=../src -include-directory=../src "$1"
echo "=== Starting second run of pdflatex with $1.tex ..."
pdflatex -file-line-error -interaction=nonstopmode -synctex=1 -output-format=pdf -include-directory=../src -shell-escape "$1".tex
pdflatex -file-line-error -interaction=nonstopmode -synctex=1 -output-format=pdf -include-directory=../src -shell-escape "$1".tex
cd .. || exit 1
